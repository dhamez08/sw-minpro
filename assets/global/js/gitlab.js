$(function(){
    var get_commits = function(){
        $.ajax({
            url: baseURL+'/ajax-get-history',
            dataType: 'json',
            success: function(response){
                $('#commit-history').html(response.view);
            }
        })
    }

    get_commits();
});