$(function(){
   $('#btn-send-chat').on('click',function(){
        var data = {
            message: $('#chat-text').val(),
            user_id: $('#user-id').val()
        }
        
        $.ajax({
            url: baseURL+'chat/ajax_send_chat',
            data: data,
            dataType: 'json',
            type: 'post',
            success: function(response){
                $('#chat-text').val('');
            }
        });
   });

   var get_chat_messages = function(){
        setTimeout(function(){
            $.ajax({
                url: baseURL+'chat/ajax_get_chat',
                dataType: 'json',
                success: function(response){
                    $('#chat-logs').html(response.view);
                    get_chat_messages();
                }
           });
        }, 1500);
   }

   get_chat_messages();
   
});