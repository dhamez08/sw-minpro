<?php

class Chat_model extends CI_Model {


    public function save($origin_user_id, $target_user_id, $message){
        $this->db
            ->insert('chat',
                array(
                        'user_id_from' => $origin_user_id,
                        'user_id_to' => $target_user_id,
                        'content' => $message,
                        'project_id' => 1
                    )
                );
        
        return $this->db->insert_id();
    }

    public function get($chat_id){
        $this->db
            ->from('chat')
            ->where('chat_id', $chat_id);
        
        $chat = $this->db->get()->row_array();

        return $chat;
    }

    public function get_messages_by_project($projec_id){
        $this->db
            ->from('chat')
            ->where('project_id', 1)
            ->order_by('chat_id');

        return $this->db->get()->result_array();
    }
}