<?php
/**
 * Created by PhpStorm.
 * User: dhamez
 * Date: 11/2/16
 * Time: 12:38 AM
 */

class Chat extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('chat_model','chat');
    }

    public function ajax_send_chat(){
        $message = $this->input->post('message');
        $origin_user_id = $this->session->userdata('user_id');
        $target_user_id = $this->input->post('user_id');

        $result = $this->chat->save($origin_user_id, $target_user_id, $message);

        if($result > 1){
            $response['view'] = '';
            $response['success'] = TRUE;
            $chat = $this->chat->get($result);

            if(!empty($chat)){
                $response['view'] = $this->load->view($this->theme.'/_partial/chat_item', $chat, TRUE);
            }
            
            echo json_encode($response);
        } else {
            echo json_encode(array('success' => FALSE));
        }
        
    }

    public function ajax_get_chat(){
        $messages = $this->chat->get_messages_by_project(1);

        $response['view'] = '';
        foreach($messages as $message){
            $response['view'] .= $this->load->view($this->theme.'/_partial/chat_item', $message, TRUE);
        }

        echo json_encode($response);
    }

    public function test(){
        pretty($this->session->all_userdata());
    }
}