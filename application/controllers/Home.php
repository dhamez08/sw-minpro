<?php
/**
 * Created by PhpStorm.
 * User: dhamez
 * Date: 11/2/16
 * Time: 12:38 AM
 */

 use Gitlab\Client;

class Home extends MY_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->layout
            ->set_js(Layout::GLOBAL_ASSET, array('home'))
            ->set_js(Layout::PLUGIN_ASSET, array(''))
            ->set_css(Layout::PLUGIN_ASSET, array(''))
            ->set_css(Layout::GLOBAL_ASSET, array(''));
    
        $data = array();
        $data['title'] = '案件一覧';
        $data['content'] = $this->load->view($this->theme.'/home_view', $data, TRUE);
        $this->load->view($this->theme.'/main_layout_view', $data);
    }

    public function join(){
        $this->layout
            ->set_js(Layout::GLOBAL_ASSET, array('home','gitlab'))
            ->set_js(Layout::PLUGIN_ASSET, array(''))
            ->set_css(Layout::PLUGIN_ASSET, array(''))
            ->set_css(Layout::GLOBAL_ASSET, array(''));    
    
        $data = array();
        $data['title'] = '案件内容';
        $data['is_sidebar_collapse'] = TRUE;
        $data['content'] = $this->load->view($this->theme.'/join_view', $data, TRUE);
        $this->load->view($this->theme.'/main_layout_view', $data);
    }

    public function commit_detail($hash = FALSE){
        $this->layout
            ->set_js(Layout::GLOBAL_ASSET, array('home','chat'))
            ->set_js(Layout::PLUGIN_ASSET, array(''))
            ->set_css(Layout::PLUGIN_ASSET, array(''))
            ->set_css(Layout::GLOBAL_ASSET, array('chat'));
    
        $data = array();


        $data['title'] = '';
        $data['chat'] = $this->load->view($this->theme.'/chat_view', $data, TRUE);
        $data['content'] = $this->load->view($this->theme.'/commit_view', $data, TRUE);
        $this->load->view($this->theme.'/main_layout_view', $data);
    }

    public function comming_soon(){
        $data['content'] = $this->load->view($this->theme.'/comming_soon_view', array(), TRUE);
        $this->load->view($this->theme.'/main_layout_view', $data);
    }

    public function ajax_get_history(){
        // $ch = curl_init('https://gitlab.com/api/v4/projects?private_token='.$this->config->item('gitlab_access_token'));
        $url = 'https://gitlab.com/api/v4/projects/'.$this->config->item('shanti_project_id').'/repository/commits?private_token='.$this->config->item('gitlab_access_token');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        $commits = json_decode($output, TRUE);

        $response['view'] = '';
        foreach(array_reverse($commits) as $commit){
            $response['view'] .= $this->load->view($this->theme.'/_partial/commit_item', $commit, TRUE);
        }   

        echo json_encode($response);
    }
}