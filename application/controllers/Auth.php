<?php
/**
 * Created by PhpStorm.
 * User: dhamez
 * Date: 11/2/16
 * Time: 12:38 AM
 */

class Auth extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('user_model','user');
    }

    public function login(){
        $this->layout
            ->set_js(Layout::GLOBAL_ASSET, array(''))
            ->set_js(Layout::PLUGIN_ASSET, array(''))
            ->set_css(Layout::PLUGIN_ASSET, array(''))
            ->set_css(Layout::GLOBAL_ASSET, array(''));
    
        $data = array();
        // $data['content'] = $this->load->view($this->theme.'/login_view', $data, TRUE);
        $this->load->view($this->theme.'/login_view', $data);
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }

    public function auth(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->user->login($email, $password);

        if(!empty($user)){
            $this->session->set_userdata($user);
            redirect('/');
        } else {
            $this->session->set_flashdata(array(
                'message' => "Invalid user",
                'type' => "error"
            ));

            redirect('login');
        }
    }
}