<?php
/**
 * Created by PhpStorm.
 * User: dhamez
 * Date: 9/9/16
 * Time: 1:55 AM
 */


if(!function_exists('issetor')){
    function issetor(&$var, $default = ""){
        return isset($var) && $var != '' ? $var : $default;
    }
}

if(!function_exists('shorten_text')){
    function shorten_text($text, $threshold = 20){
        if(strlen($text) > $threshold){
            $temp_text =  substr($text, 0, $threshold-1);

            $last_period = strripos($temp_text,'.');
            $last_space = strripos($temp_text,' ');
            if ($last_space > $last_period) {
                $shortened_text = substr($temp_text,0,$last_space) . '...';
            } else {
                $shortened_text = substr($temp_text,0,$last_period);
            }

            return $shortened_text;
        } else {
            return $text;
        }
    }
}

if(!function_exists('pretty')){
    function pretty($data){
        echo "<fieldset><legend>Data</legend><pre>",print_r($data, TRUE),"</pre></fieldset>";
    }
}

if(!function_exists('yes_no')){
    function yes_no(&$data){
        if(isset($data) AND $data){
            return 'Yes';
        } else {
            return 'No';
        }
    }
}

if(!function_exists('isset_true')){
    function isset_true(&$data){
        if(isset($data) AND $data){
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

if(!function_exists('matched_select')){
    function matched_select(&$variable, $value, $display, $default = false){
        if(isset($variable)){
            if($variable == $value){
                return $display;
            }
        } else {
            if($default){
                return $display;
            }
        }
    }
}

if(!function_exists('random_key')){
    function random_key($length) {
        $pool = array_merge(range(0,9));
        $key = '';
        
        for($i=0; $i < $length; $i++) {
            $key .= $pool[mt_rand(0, count($pool) - 1)];
        }
        return $key;
    }
}


if(!function_exists('random_alphanumeric_key')){
    function random_alphanumeric_key($length) {
        $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));
        $key = '';
        for($i=0; $i < $length; $i++) {
            $key .= $pool[mt_rand(0, count($pool) - 1)];
        }
        return $key;
    }
}

if(!function_exists('current_full_url')){
    function current_full_url(){
        $CI =& get_instance();
    
        $url = $CI->config->site_url($CI->uri->uri_string());
        return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
    }
}

if(!function_exists('img_resize')){
    function img_resize($source_path, $target_path){
        $CI =& get_instance();
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 250,
            'height' => 250
        );
        $CI->load->library('image_lib', $config_manip);
        if (!$CI->image_lib->resize()) {
            echo $CI->image_lib->display_errors();
        }
        $CI->image_lib->clear();

        return $target_path;
    }
}

