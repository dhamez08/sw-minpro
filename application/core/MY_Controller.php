<?php

class MY_Controller extends CI_Controller {
    protected $theme;

    public function __construct(){
        parent::__construct();
        $this->theme = $this->config->item('theme');
    }
}