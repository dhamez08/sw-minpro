<style>
    .comming-soon-logo {
        margin: 0 auto;
        text-align: center;
        padding-top: 100px;
    }

    .comming-soon-logo img {
        width: 50%;
    }
</style>

<div class="comming-soon-logo">
    <img src="<?=base_url('assets/global/img/commingsoon.png')?>">
</div>