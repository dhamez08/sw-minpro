<div id="chat-frame">
    <div id="chat-logs">
        
    </div>
</div>
<div class="chat-textarea mgt-15">
    <div class="row">
        <div class="col-md-12">
            <input type="hidden" id="user-id" value="<?= $this->session->userdata('user_id') == 1 ? 2 : 1?>" />
            <textarea class="form-control" id="chat-text"></textarea>
            <button class="btn btn-success mgt-15" id="btn-send-chat">Send</button>
        </div>
    </div>
</div>