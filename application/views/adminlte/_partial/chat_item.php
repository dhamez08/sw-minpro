<?php 
    $current_user_id = $this->session->userdata('user_id');

    if($user_id_from == 1){
        $user_icon = 'assets/global/img/tristan.jpg';
    } else {
        $user_icon = 'assets/global/img/yume.png';
    }

    if($current_user_id == $user_id_from){
        $my_talk = 'mytalk';
    } else {
        $my_talk = '';
    }
?>

<p class="chat-talk <?=$my_talk?>">
    <span class="talk-icon">
        <img src="<?=$user_icon?>" alt="myicon" width="XX" height="XX"/>
    </span>
    <span class="talk-content"><?=$content?></span>
</p>