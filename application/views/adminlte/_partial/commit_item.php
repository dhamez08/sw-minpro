<div class="row">
    <div class="col-md-12 commit-item">
        <div class="commit-photo pull-left" style="background-image: url('<?=base_url('assets/global/img/no_pic_3.jpg')?>')"></div>
        <div class="commit-detail pull-left">
            <div>
                <strong><a href="<?=base_url('commit')?>"><?=$title?></a></strong>
            </div>
            <div><?=$author_name?> <?=$committed_date?></div>
        </div>
        <div class="commit-hash pull-right"><strong><a href="<?=base_url('commit')?>"><?=$short_id?></a></strong></div>
    </div>
</div>