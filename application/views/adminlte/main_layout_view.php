<!DOCTYPE html>
<html>
<head>
    <base href="<?=base_url()?>"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ShiSho</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="assets/plugin/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="assets/adminlte/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="assets/adminlte/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="assets/global/css/style.css">
    <link rel="stylesheet" href="assets/global/css/helpers.css">
    <link rel="stylesheet" href="assets/plugin/sweetalert/sweetalert.css">
    <!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css"> -->
    <link rel="stylesheet" href="assets/plugin/highlight/styles/default.css">

    <?php $this->layout->display_css(Layout::THEME_ASSET) ?>

    <?php $this->layout->display_css(Layout::PLUGIN_ASSET) ?>

    <?php $this->layout->display_css(Layout::GLOBAL_ASSET) ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>var baseURL = '<?=base_url()?>'</script>
    <!-- jQuery 2.2.3 -->
    <script src="assets/plugin/jQuery/jquery-2.2.3.min.js"></script>
</head>
<?php 
    $collapse_class = issetor($is_sidebar_collapse, FALSE) ? "sidebar-collapse" : "";
?>
<body class="hold-transition skin-green sidebar-mini <?=$collapse_class?>">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="assets/index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>S</b>S</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="<?=base_url('assets/global/img/shisho_logo.png')?>"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <?php
                        if($this->session->userdata('user_id') == 1){
                            $user_icon = 'assets/global/img/tristan.jpg';
                        } else {
                            $user_icon = 'assets/global/img/yume.png';
                        }
                    
                    ?>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            
                            <div class="user-image photo-preview" style="background-image: url('<?=base_url($user_icon)?>')"></div>
                            <span class="hidden-xs"><?=$this->session->userdata('first_name')?> <?=$this->session->userdata('last_name')?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
<!--                                <img src="assets/adminlte/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
                                <div class="profile-photo center-div" style="background-image: url('<?=base_url($user_icon)?>')"></div>
                                <p>
                                    <?=$this->session->userdata('first_name')?> <?=$this->session->userdata('last_name')?>
                                    <small>Member since <?=date('F Y', strtotime($this->session->userdata('date_created')))?></small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?=base_url('user/profile')?>" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?=base_url('logout')?>" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel hide">
                <div class="pull-left image">
                    <div class="img-circle pp-35 photo-preview" style="background-image: url('<?=base_url($this->session->userdata('photo'))?>')"></div>
                </div>
                <div class="pull-left info">
                    <p><?=$this->session->userdata('first_name')?> <?=$this->session->userdata('last_name')?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <ul class="sidebar-menu">
                <li class="header">メニュー一覧</li>
                <li><a href="<?=base_url('home')?>"><i class="fa fa-dashboard"></i> <span>案件一覧</span></a></li>
                <li><a href="<?=base_url('comming-soon')?>"><i class="fa fa-dashboard"></i> <span>中級コンテンツ</span></a></li>
                <li><a href="<?=base_url('comming-soon')?>"><i class="fa fa-dashboard"></i> <span>HowTo</span></a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?=issetor($title)?>
            </h1>
        </section>

        <!-- Main content -->
        <?=issetor($content)?>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2016-2017 <a href="#">ShiSho</a>.</strong> All rights
        reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>

            <li class="hide"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
            <form method="post">
            <input type="hidden" name="id" value="<?=issetor($company['id'])?>"/>
            <h3 class="control-sidebar-heading">General Settings</h3>
            <div class="pmi-general-settings">
                <div class="form-group row">
                    <label class="col-sm-6 control-label">Enable Commission</label>
                    <div class="col-sm-6">
                        <input type="checkbox" class="enable-commission" <?=matched_select($company['has_commission'],1,'checked')?>>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-6 control-label">Enable Discounts</label>
                    <div class="col-sm-6">
                        <input type="checkbox" class="enable-discount" <?=matched_select($company['display_discount'],1,'checked')?>>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-6 control-label">Commission</label>
                    <div class="col-sm-6">
                        <?php if(!isset_true($company['id'])): ?>
                            <label class="radio inline control-label text-left-force"><em>Save company name first before you can create commision.</em></label>
                        <?php else: ?>
                            <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#commission-modal">Configure</button>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-6 control-label">Discounts</label>
                    <div class="col-sm-6">
                        <?php if(!isset_true($company['id'])): ?>
                            <label class="radio inline control-label text-left-force"><em>Save company name first before you can create discounts.</em></label>
                        <?php else: ?>
                            <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#discount-modal">Configure</button>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-6 control-label">Presets</label>
                    <div class="col-sm-6">
                        <?php if(!isset_true($company['id'])): ?>
                            <label class="radio inline control-label text-left-force"><em>Save company name first before you can create presets.</em></label>
                        <?php else: ?>
                            <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#preset-modal">Configure</button>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-6 control-label">Benefits</label>
                    <div class="col-sm-6">
                        <?php if(!isset_true($company['id'])): ?>
                            <label class="radio inline control-label text-left-force"><em>Save company name first before you can create presets.</em></label>
                        <?php else: ?>
                            <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#benefit-setting-modal">Configure</button>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-6 control-label">Reasons</label>
                    <div class="col-sm-6">
                        <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#reason-setting-modal">Configure</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-6 control-label">Company PMI Sender List</label>
                    <div class="col-sm-6">
                        <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#senders-list-modal">Configure</button>
                    </div>
                </div>
            </div>
            <!-- /.form-group -->
        </form>

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<script src="assets/plugin/highlight/highlight.pack.js">
<script src="https://getaddress.io/js/jquery.getAddress-1.0.1.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="assets/plugin/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="assets/plugin/fastclick/fastclick.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>
<script src="assets/plugin/sweetalert/sweetalert.min.js"></script>
<script src="assets/global/js/keep-alive.js"></script>
<script src="assets/global/js/main.js"></script>
<?php $this->layout->display_js(Layout::THEME_ASSET) ?>

<?php $this->layout->display_js(Layout::PLUGIN_ASSET) ?>
<!-- AdminLTE App -->
<script src="assets/adminlte/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/adminlte/js/demo.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

<?php $this->layout->display_js(Layout::GLOBAL_ASSET) ?>
</body>
</html>
