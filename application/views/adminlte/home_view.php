<div class="content">
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">LP作成</h3>
                </div>
                <div class="box-body">
                    <p>案件オーナー：株式会社シャンティ様</p>
                    <p>必要スキル：HTML5,javascript,CSS3</p>
                    <p>期間：2018/7/18～2018/8/18</p>
                    <P>難易度：★★★</p>
                </div>
                <div class="box-footer">
                    <button class="btn btn-success form-control btn-card">修行開始！</button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
            <div class="box-header with-border">
                    <h3 class="box-title">kintone開発</h3>
                </div>
                <div class="box-body">
                    <p>案件オーナー：株式会社Esyousystem様</p>
                    <p>必要スキル：javascript</p>
                    <p>期間：2018/8/1～2018/8/31</p>
                    <P>難易度：★★★★★</p>
                </div>
                <div class="box-footer">
                    <button class="btn btn-success form-control btn-card">修行開始！</button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Unity環境構築</h3>
                </div>
                <div class="box-body">
                    <p>案件オーナー：yamadan様</p>
                    <p>必要スキル：Unity + Kinect環境構築</p>
                    <p>期間：2018/7/24～2018/8/18</p>
                    <P>難易度：★★★★☆</p>
                </div>
                <div class="box-footer">
                    <button class="btn btn-success form-control btn-card">修行開始！</button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">LP作成</h3>
                </div>
                <div class="box-body">
                    <p>案件オーナー：株式会社シャンティ様</p>
                    <p>必要スキル：HTML5,javascript,CSS3</p>
                    <p>期間：2018/7/18～2018/8/18</p>
                    <P>難易度：★★★</p>
                </div>
                <div class="box-footer">
                    <button class="btn btn-success form-control btn-card">修行開始！</button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
            <div class="box-header with-border">
                    <h3 class="box-title">kintone開発</h3>
                </div>
                <div class="box-body">
                    <p>案件オーナー：株式会社Esyousystem様</p>
                    <p>必要スキル：javascript</p>
                    <p>期間：2018/8/1～2018/8/31</p>
                    <P>難易度：★★★★★</p>
                </div>
                <div class="box-footer">
                    <button class="btn btn-success form-control btn-card">修行開始！</button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Unity環境構築</h3>
                </div>
                <div class="box-body">
                    <p>案件オーナー：yamadan様</p>
                    <p>必要スキル：Unity + Kinect環境構築</p>
                    <p>期間：2018/7/24～2018/8/18</p>
                    <P>難易度：★★★★☆</p>
                </div>
                <div class="box-footer">
                    <button class="btn btn-success form-control btn-card">修行開始！</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="card-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">LP作成</h5>
        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
        <p>案件オーナー：株式会社シャンティ様</p>
        <p>必要スキル：HTML5,javascript,CSS3</p>
        <p>期間：2018/7/18～2018/8/18</p>
        <P>難易度：★★★</p>
        <P>内容：</p>
        <P>医療系のコンテンツのため、明るくて優しい色使いで作成してください。</p>
      </div>
      <div class="modal-footer">
        <a class="btn btn-success form-control btn-join" href="<?=base_url('join')?>">JOIN</a>
      </div>
    </div>
  </div>
</div>