<!DOCTYPE html>
<html>
<head>
    <base href="<?=base_url()?>"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MinPro</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="assets/plugin/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="assets/adminlte/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="assets/adminlte/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="assets/global/css/style.css">
    <link rel="stylesheet" href="assets/global/css/helpers.css">
    <link rel="stylesheet" href="assets/plugin/sweetalert/sweetalert.css">

    <?php $this->layout->display_css(Layout::THEME_ASSET) ?>

    <?php $this->layout->display_css(Layout::PLUGIN_ASSET) ?>

    <?php $this->layout->display_css(Layout::GLOBAL_ASSET) ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>var baseURL = '<?=base_url()?>'</script>
    <!-- jQuery 2.2.3 -->
    <script src="assets/plugin/jQuery/jquery-2.2.3.min.js"></script>
</head>
<body class="hold-transition skin-green sidebar-mini">
<?=issetor($content)?>

<script src="https://getaddress.io/js/jquery.getAddress-1.0.1.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="assets/plugin/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="assets/plugin/fastclick/fastclick.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>
<script src="assets/plugin/sweetalert/sweetalert.min.js"></script>
<script src="assets/global/js/keep-alive.js"></script>
<script src="assets/global/js/main.js"></script>
<?php $this->layout->display_js(Layout::THEME_ASSET) ?>

<?php $this->layout->display_js(Layout::PLUGIN_ASSET) ?>
<!-- AdminLTE App -->
<script src="assets/adminlte/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/adminlte/js/demo.js"></script>

<?php $this->layout->display_js(Layout::GLOBAL_ASSET) ?>
</body>
</html>
