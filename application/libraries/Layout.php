<?php
/**
 * Created by PhpStorm.
 * User: dhamez
 * Date: 11/21/16
 * Time: 11:50 PM
 */
class Layout {
    const THEME_ASSET = 'theme';
    const GLOBAL_ASSET = 'global';
    const PLUGIN_ASSET = 'plugin';
    
    protected $css;
    protected $js;
    protected $ci;
    protected $theme;
    
    public function __construct()
    {
        $this->css = array();
        $this->js = array();
        $this->ci =& get_instance();
        $this->theme = $this->ci->config->item('theme');
    }

    public function set_css($type, $styles){
        $this->css[$type] = array_merge(issetor($this->css[$type], array()),$styles);
        return $this;
    }
    
    public function set_js($type, $scripts){
        $this->js[$type] = array_merge(issetor($this->js[$type], array()), $scripts);
        return $this;
    }
    
    public function display_css($type){
        if(isset($this->css[$type])) {
            foreach ($this->css[$type] as $style) {
                if($type == Layout::THEME_ASSET){
                    echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"assets/{$this->theme}/css/{$style}.css\" />\n";
                } else if($type == Layout::PLUGIN_ASSET){
                    echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"assets/{$type}/{$style}.css\" />\n";
                } else {
                    echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"assets/{$type}/css/{$style}.css\" />\n";
                }
            }
        }
    }
    
    public function display_js($type){
        if(isset($this->js[$type])) {
            foreach ($this->js[$type] as $script) {
                if($type == Layout::THEME_ASSET){
                    echo "<script type=\"text/javascript\" src=\"assets/{$this->theme}/js/{$script}.js\"></script>\n";
                } else if($type == Layout::PLUGIN_ASSET){
                    echo "<script type=\"text/javascript\" src=\"assets/{$type}/{$script}.js\"></script>\n";
                } else {
                    echo "<script type=\"text/javascript\" src=\"assets/{$type}/js/{$script}.js\"></script>\n";
                }
                
            }
        }
    }
    
}